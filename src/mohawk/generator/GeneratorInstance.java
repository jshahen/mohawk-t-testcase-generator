package mohawk.generator;

import java.io.*;
import java.util.logging.*;

import org.apache.commons.cli.*;
import org.apache.commons.lang3.StringUtils;

import mohawk.global.formatter.MohawkCSVFileFormatter;
import mohawk.global.formatter.MohawkConsoleFormatter;
import mohawk.global.generators.ASAPTimeSARandomGenerator;
import mohawk.global.generators.MohawkTRandomGenerator;
import mohawk.global.helper.FileExtensions;
import mohawk.global.timing.MohawkTiming;

public class GeneratorInstance {
    private static final String VERSION = "v0.0.2";
    private static final String AUTHORS = "Jonathan Shahen <jmshahen@uwaterloo.ca>";

    // Logger Fields
    public static final Logger logger = Logger.getLogger("mohawk");
    private String Logger_filepath = "mohawk-generator.log";
    private String Logger_folderpath = "logs";
    private ConsoleHandler consoleHandler = new ConsoleHandler();
    private Level LoggerLevel;
    private Boolean WriteCSVFileHeader = true;
    private FileHandler fileHandler;

    // Testcase Fields
    private FileWriter testcaseWriter;
    private File testcaseFile;
    public String testcaseComment = "";

    // Helpers
    public FileExtensions fileExt = new FileExtensions();
    public MohawkTiming timing = new MohawkTiming();
    private String timingFile = "logs/latest_Mohawk-T_Generator_Timing_Results.csv";

    // Actionable Fields
    public String action = "";
    public Integer numberOfRoles;
    public Integer numberOfRules;
    public Integer numberOfTimeslots;
    public int repeat = 10;

    public Integer run(String[] args) {
        try {
            /* ********* DEAL WITH OPTIONS ********* */
            Options options = new Options();
            setupOptions(options);

            CommandLineParser cmdParser = new BasicParser();
            CommandLine cmd = cmdParser.parse(options, args);

            setupLoggerOptions(cmd, options);

            if (setupReturnImmediatelyOptions(cmd, options)) { return 0; }

            setupUserPreferenceOptions(cmd, options);

            setupResultOptions(cmd, options);

            setupActionableOptions(cmd, options);

            /* ********* DONE WITH OPTIONS ********* */

            /* Timing */timing.startTimer("totalTime");

            for (int i = 1; i <= repeat; i++) {
                /* Timing */timing.startTimer("loop (" + i + ")");
                if (cmd.hasOption("random")) {
                    /* Timing */timing.startTimer("loop (" + i + ") random");
                    logger.info("[GENERATOR] Running the Mohawk-T Random Generator");
                    MohawkTRandomGenerator randGen = new MohawkTRandomGenerator();

                    if (cmd.hasOption("randomPre")) {
                        logger.info("[OPTION] Setting the Random Precondition Setting to: "
                                + cmd.getOptionValue("randomPre"));
                        randGen.preconditionGen = cmd.getOptionValue("randomPre");
                    }

                    randGen.generate(numberOfRoles, numberOfTimeslots, numberOfRules);
                    randGen.lastTestcase.comment = testcaseComment;

                    logger.info("[GENERATOR] Successfully generated the random Testcase");
                    String generatedTestcase = randGen.lastTestcase.getString("\n\n", true, true);

                    testcaseFile = new File("random_role" + numberOfRoles + "_rule" + numberOfRules + "_time"
                            + numberOfTimeslots + "_" + i + fileExt.Mohawk_T);
                    logger.info("[GENERATOR] Writing the Random Testcase to file: " + testcaseFile);
                    testcaseWriter = new FileWriter(testcaseFile, false);
                    testcaseWriter.write(generatedTestcase);
                    testcaseWriter.close();
                    logger.info("[GENERATOR] Done Writing the Testcase to the file");
                    /* Timing */timing.stopTimer("loop (" + i + ") random");
                }
                if (cmd.hasOption("randomSA")) {
                    /* Timing */timing.startTimer("loop (" + i + ") randomSA");
                    logger.info("[GENERATOR] Running the ASAPTime SA Random Generator");
                    ASAPTimeSARandomGenerator randGen = new ASAPTimeSARandomGenerator();

                    if (cmd.hasOption("randomPre")) {
                        logger.info("[OPTION] Setting the Random Precondition Setting to: "
                                + cmd.getOptionValue("randomPre"));
                        randGen.preconditionGen = cmd.getOptionValue("randomPre");
                    }

                    randGen.generate(numberOfRoles, numberOfTimeslots, numberOfRules);
                    randGen.lastTestcase.comment = testcaseComment;

                    logger.info("[GENERATOR] Successfully generated the random Testcase");
                    String generatedTestcase = randGen.lastTestcase.getString();

                    testcaseFile = new File("random_role" + numberOfRoles + "_rule" + numberOfRules + "_time"
                            + numberOfTimeslots + "_" + i + fileExt.ASASPTime_SA);
                    logger.info("[GENERATOR] Writing the Random Testcase to file: " + testcaseFile);
                    testcaseWriter = new FileWriter(testcaseFile, false);
                    testcaseWriter.write(generatedTestcase);
                    testcaseWriter.close();
                    logger.info("[GENERATOR] Done Writing the Testcase to the file");
                    /* Timing */timing.stopTimer("loop (" + i + ") randomSA");
                }

                /* Timing */timing.stopTimer("loop (" + i + ")");
            }

            /* Timing */timing.stopTimer("totalTime");
            /* Timing */timing.writeOut(new File(timingFile));

            logger.info(timing.toString());

        } catch (Exception e) {
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));
            logger.severe(errors.toString());
            logger.severe(e.getMessage());
        }

        logger.info("Generator Instance done running");
        return 0;
    }

    public void printHelp(CommandLine cmd, Options options) throws Exception {
        if (cmd.hasOption("maxw")) {
            try {
                Integer maxw = Integer.decode(cmd.getOptionValue("maxw"));
                printHelp(options, maxw);
            } catch (Exception e) {
                printHelp(options, 80);

                e.printStackTrace();
                throw new Exception("An error occured when trying to print out the help options!");
            }
        } else {
            printHelp(options, 80);
        }
    }

    public void printHelp(Options options, int maxw) {
        HelpFormatter f = new HelpFormatter();
        f.printHelp(maxw, "mohawk",
                StringUtils.repeat("-", maxw) + "\nAuthors: " + AUTHORS + "\n" + StringUtils.repeat("-", 20), options,
                StringUtils.repeat("-", maxw), true);
    }

    @SuppressWarnings("static-access")
    public void setupOptions(Options options) {
        // Add Information Options
        options.addOption("help", false, "Print this message");
        options.addOption("authors", false, "Prints the authors");
        options.addOption("version", false, "Prints the version (" + VERSION + ") information");

        // Add Logging Level Options
        options.addOption(OptionBuilder.withArgName("quiet|debug|verbose")
                .withDescription("Be extra quiet only errors are shown; " + "Show debugging information; "
                        + "extra information is given for Verbose; " + "default is warning level")
                .hasArg().create("loglevel"));
        options.addOption(OptionBuilder.withArgName("logfile|'n'|'u'")
                .withDescription("The filepath where the log file should be created; "
                        + "No file will be created when equal to 'n'; "
                        + "A unique filename will be created when equal to 'u'; " + "default it creates a log called '"
                        + Logger_filepath + "'")
                .hasArg().create("logfile"));
        options.addOption("noheader", false, "Does not write the CSV file header to the output log");

        options.addOption(OptionBuilder.withArgName("csvfile")
                .withDescription("The file where the result should be stored").hasArg().create("results"));

        // custom Console Logging Options
        options.addOption(OptionBuilder.withArgName("num")
                .withDescription("The maximum width of the console (default 120)").hasArg().create("maxw"));
        options.addOption(OptionBuilder.withArgName("string")
                .withDescription("The new line string when wrapping a long line (default '\\n    ')").hasArg()
                .create("linestr"));

        // Add File IO Options
        options.addOption(OptionBuilder.withArgName("file")
                .withDescription("Path to the where the Generated file should be written").hasArg().create("output"));

        // Testcase Options
        options.addOption(OptionBuilder.withArgName("text")
                .withDescription(
                        "Comment to be written into the output file. Use quotation marks if your comment has spaces.")
                .hasArg().create("comment"));

        // Actionable Options
        options.addOption("random", false, "Creates a random testcase using the Mohawk-T Random Generator ("
                + MohawkTRandomGenerator.VERSION + ")");
        options.addOption("randomSA", false, "Creates a random testcase using the ASAPTime SA Random Generator ("
                + MohawkTRandomGenerator.VERSION + ")");
        options.addOption(OptionBuilder.withArgName("number")
                .withDescription("Number of Rules to use when generating Testcase").hasArg().create("rules"));
        options.addOption(OptionBuilder.withArgName("number")
                .withDescription("Number of Roles to use when generating Testcase").hasArg().create("roles"));
        options.addOption(OptionBuilder.withArgName("number")
                .withDescription("Number of Timeslots to use when generating Testcase").hasArg().create("timeslots"));

        options.addOption(OptionBuilder.withArgName("num").withDescription("Repeat Count (Default: " + repeat + ")")
                .hasArg().create("repeat"));

        options.addOption(OptionBuilder.withArgName("string")
                .withDescription("Creates a random testcase using some porportion for the random preconditions. "
                        + "Please see the Javadocs or source code for the specifics")
                .hasArg().create("randomPre"));

    }

    public Level getLoggerLevel() {
        return LoggerLevel;
    }

    public void setLoggerLevel(Level loggerLevel) {
        LoggerLevel = loggerLevel;
    }

    private void setupActionableOptions(CommandLine cmd, Options options) throws Exception {
        if ((cmd.hasOption("random") || cmd.hasOption("randomSA")) && cmd.hasOption("rules") && cmd.hasOption("roles")
                && cmd.hasOption("timeslots")) {
            // Generate a Random Testcase
            try {
                numberOfRoles = Integer.decode(cmd.getOptionValue("roles"));
                numberOfRules = Integer.decode(cmd.getOptionValue("rules"));
                numberOfTimeslots = Integer.decode(cmd.getOptionValue("timeslots"));
            } catch (NumberFormatException e) {
                printHelp(options, 80);
                e.printStackTrace();
                throw new Exception("An error occured when trying to decode an Integer value");
            }
        }

        if (cmd.hasOption("repeat")) {
            try {
                repeat = Integer.decode(cmd.getOptionValue("repeat"));
            } catch (NumberFormatException e) {
                printHelp(options, 80);
                e.printStackTrace();
                throw new Exception("An error occured when trying to decode an Integer value");
            }
        }
    }

    private void setupLoggerOptions(CommandLine cmd, Options options) throws SecurityException, IOException {
        // Logging Level
        logger.setUseParentHandlers(false);
        consoleHandler.setFormatter(new MohawkConsoleFormatter());
        setLoggerLevel(Level.INFO); // Default Level
        if (cmd.hasOption("loglevel")) {
            String loglevel = cmd.getOptionValue("loglevel");
            if (loglevel.equalsIgnoreCase("quiet")) {
                setLoggerLevel(Level.SEVERE);
            } else if (loglevel.equalsIgnoreCase("debug")) {
                setLoggerLevel(Level.FINEST);
            } else if (loglevel.equalsIgnoreCase("verbose")) {
                setLoggerLevel(Level.INFO);
            }
        }

        logger.setLevel(LoggerLevel);
        consoleHandler.setLevel(LoggerLevel);
        logger.addHandler(consoleHandler);

        // Add CSV File Headers
        if (cmd.hasOption("noheader")) {
            WriteCSVFileHeader = false;
        }

        // Set Logger Folder
        if (cmd.hasOption("logfolder")) {
            File logfile = new File(cmd.getOptionValue("logfolder"));

            if (!logfile.exists()) {
                logfile.mkdir();
            }

            if (logfile.isDirectory()) {
                Logger_folderpath = cmd.getOptionValue("logfolder");
            } else {
                logger.severe("logfolder did not contain a folder that exists or that could be created!");
            }
        }

        // Set File Logger
        if (cmd.hasOption("logfile")) {
            // Check if no log file was requested
            if (cmd.getOptionValue("logfile").equals("n")) {
                // Create no log file
                Logger_filepath = "";
            } else if (cmd.getOptionValue("logfile").equals("u")) {
                // Create a unique log file
                Logger_filepath = "mohawk-log.%u.%g.txt";
            } else {
                try {
                    // Create a log file with a specific name
                    File logfile = new File(Logger_folderpath + File.separator + cmd.getOptionValue("logfile"));

                    if (!logfile.exists()) {
                        logfile.createNewFile();
                    }
                    Logger_filepath = logfile.getAbsolutePath();

                    if (WriteCSVFileHeader) {
                        FileOutputStream writer = new FileOutputStream(logfile, true); // Always append!
                        writer.write(MohawkCSVFileFormatter.csvHeaders().getBytes());
                        writer.flush();
                        writer.close();
                    }

                } catch (IOException e) {
                    logger.severe(e.getMessage());
                    return;
                }
            }
        }
        // Add Logger File Handler
        if (!Logger_filepath.isEmpty()) {
            fileHandler = new FileHandler(Logger_folderpath + File.separator + Logger_filepath);
            fileHandler.setLevel(getLoggerLevel());
            fileHandler.setFormatter(new MohawkCSVFileFormatter());
            logger.addHandler(fileHandler);
        }
    }

    private Boolean setupReturnImmediatelyOptions(CommandLine cmd, Options options) throws Exception {
        if (cmd.hasOption("help") == true || cmd.getOptions().length == 0) {
            printHelp(cmd, options);
            return true;
        }

        if (cmd.hasOption("version")) {
            // keep it as simple as possible for the version
            System.out.println(VERSION);
            return true;
        }

        if (cmd.hasOption("authors")) {
            // keep it as simple as possible for the version
            System.out.println(AUTHORS);
            return true;
        }

        return false;
    }

    private void setupUserPreferenceOptions(CommandLine cmd, Options options) {
        // Set the Console's Max Width
        if (cmd.hasOption("maxw")) {
            logger.fine("[OPTION] Setting the console's maximum width");
            String maxw = "";
            try {
                maxw = cmd.getOptionValue("maxw");
                ((MohawkConsoleFormatter) consoleHandler.getFormatter()).maxWidth = Integer.decode(maxw);
            } catch (NumberFormatException e) {
                logger.severe("[ERROR] Could not decode 'maxw': " + maxw + ";\n" + e.getMessage());
            }
        } else {
            logger.fine("[OPTION] Default Console Maximum Width Used");
        }

        // Set the Console's Wrap String
        if (cmd.hasOption("linestr")) {
            logger.fine("[OPTION] Setting the console's new line string");
            ((MohawkConsoleFormatter) consoleHandler.getFormatter()).newLineStr = cmd.getOptionValue("linestr");
        } else {
            logger.fine("[OPTION] Default Line String Used");
        }

        // Set the Console's Wrap String
        if (cmd.hasOption("comment")) {
            testcaseComment = cmd.getOptionValue("comment");
            logger.info("[OPTION] Setting custom comment for file: " + testcaseComment);
        }
    }

    private void setupResultOptions(CommandLine cmd, Options options) throws IOException {}
}
