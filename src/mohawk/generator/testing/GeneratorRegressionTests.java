package mohawk.generator.testing;

import static org.junit.Assert.assertEquals;
import mohawk.global.generators.MohawkTRandomGenerator;

import org.junit.Test;

public class GeneratorRegressionTests {

    @Test
    public void randomGeneratorSmall() {
        Integer NUMBER_OF_RULES = 100;
        Integer NUMBER_OF_TIME_SLOTS = 20;
        Integer NUMBER_OF_ROLES = 40;

        MohawkTRandomGenerator randGen = new MohawkTRandomGenerator();

        randGen.generate(NUMBER_OF_ROLES, NUMBER_OF_TIME_SLOTS, NUMBER_OF_RULES);

        System.out.println(randGen.lastTestcase.getString("\n\n", true, true));
        // System.out.println(randGen.testcase.roleHelper.toString());

        assertEquals("Did not create the correct number of rules!", NUMBER_OF_RULES,
                randGen.lastTestcase.numberOfRules());
        // assertEquals("Did not create the correct number of roles!", NUMBER_OF_ROLES,
        // randGen.lastTestcase.numberOfRoles());
        // assertEquals("Did not create the correct number of timeslots!", NUMBER_OF_TIME_SLOTS,
        // randGen.lastTestcase.numberOfTimeslots());
    }
}
