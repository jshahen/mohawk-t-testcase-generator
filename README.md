# Description 


# How To Use
1. Best way to get working with the code is to open it in Eclipse
2. You will need to have Java 1.8 installed and Ant
3. Load it up in eclipse and go to the Ant window (Window -> Show View -> Ant) and click on "Run the Default Target"  
	> If you do not want to use Eclipse, just run "ant build.xml"
4. 


# Contact
Author: Jonathan Shahen <jmshahen@uwaterloo.ca>

# Notes
* 